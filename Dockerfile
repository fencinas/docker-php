ARG PHP_VERSION=7.3-apache

FROM php:${PHP_VERSION}

RUN docker-php-ext-install mysqli pdo pdo_mysql
RUN a2enmod rewrite
